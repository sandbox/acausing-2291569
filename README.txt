CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements & Installation
 * Configuration
 * How It Works: Scenario
 * Advantages
 * Similar Modules
 * Todo List
 * Maintainers

INTRODUCTION
------------
This module renders the field collection as slide-item elements for
jquery.flexslider.js and jquery.fullPage.js (aim to use more sliders in
the future) by auto generating a block that can be used anywhere.

REQUIREMENTS & INSTALLATION
---------------------------
1. Download and Install the following:
   https://www.drupal.org/project/field_collection
   https://www.drupal.org/project/entity
   https://www.drupal.org/project/field_collection_slideshow
2. Download additional libraries
   * FlexSlider 
     - Download at https://github.com/woothemes/FlexSlider/zipball/master
   * fullPage.js 
     - Download at https://github.com/alvarotrigo/fullPage.js/zipball/master
   * Swiper
     - Download at https://github.com/nolimits4web/Swiper/zipball/master
   * Animated Responsive Image Grid
     - Download at https://github.com/codrops/AnimatedResponsiveImageGrid

   Extract the javascript libraries into /sites/all/libraries

CONFIGURATION
-------------
1. Create a content type and add a field collection
2. Add fields of the created field collection 
   at http://yoursite/admin/structure/field-collections
3. Enable at http://yoursite/admin/structure/field-collections/slideshow
4. Manage your block page http://yoursite/admin/structure/block and assign
   the generated block on the region you want. 
   The block name starts with "[FC-Slideshow]" prefix.
5. Add css/styling if you have multiple items in field_collection.


HOW IT WORKS: SCENARIO
----------------------
Consider you need 5 slideshows in different pages.
1. Create a content type: Let's call it "Slideshows Configuration Page"
2. Add a Field Collection Field with field_slide1, field_slide2, field_slide3, 
   field_slide4 and field_slide5
3. Configure each field_slideX in field_collection admin page.
4. Go to Field Collection Slideshow and enable them by ticking the 
   checkbox which field_collection field you want to enable.
5. You will have automatically 5 blocks that you can use anywhere.
   Each is already a slideshow that uses flexslider or fullpage JS library.

ADVANTAGES
----------
Making use of field-collection as slides:
1. Easy to Add and Remove field-collection item.
2. Easy to Sort Items
3. Editing the all field-collection items in single page 
   (versus content type that you have to add 1-by-1, 
   hard to manage if you have to renew all images and text)

SIMILAR MODULES
---------------
Views Slideshow - If you want to use the default jQuery sliding feature.
FlexSlider Field Collection - using FlexSlider
Field Slideshow - using jQuery Cycle

TODO LIST
---------
- More control on specific slide library
- Support to field groups and retain sorting and groupings
- Add more Slider Library support:
-- Swiper
-- Animated Responsive Image Grid
-- Elastic Image Slideshow with Thumbnail Preview
-- Camera
-- Item Slider

MAINTAINER
----------
Albert S. Causing (acausing) - https://www.drupal.org/user/843564
