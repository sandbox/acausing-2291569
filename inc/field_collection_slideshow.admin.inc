<?php

/**
 * @file
 * Field Collection Slideshow admin ui file.
 */

/**
 * Field Collection Slideshow admin function.
 *
 * @return string
 *   Admin form where to enable field_collection to be a sldieshow.
 */
function field_collection_slideshow_admin() {
  $options = array();
  foreach (libraries_info() as $key => $value) {
    if ($value['module'] == 'field_collection_slideshow') {
      if (libraries_get_path($key)) {
        $options[$key] = $key;
      }
    }
  }
  $form = array();

  $form['fcs_enabled_to'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable slideshow on the following:'),
    '#description' => t('Only content type with field collection will show in the list and allowed to be enabled. <br />
           Each enabled content type will automatically have a slideshow blocks. <br />
           Please disable/hide your field_collection field on Manage Display and use the generated block.<br />'),
  );

  foreach (field_collection_slideshow_admin_checkboxes() as $content_type => $field_collection_names) {
    $form['fcs_enabled_to'][$content_type] = array(
      '#type' => 'fieldset',
      '#title' => $content_type,
    );
    foreach ($field_collection_names as $field_collection_name) {
      $field_name = $content_type . '_' . $field_collection_name;
      $form['fcs_enabled_to'][$content_type][$field_name] = array(
        '#type' => 'checkbox',
        '#title' => "<strong>" . $content_type . "</strong> using " . $field_collection_name,
        '#default_value' => variable_get($field_name, ''),
        '#required' => FALSE,
      );
      $form['fcs_enabled_to'][$content_type][$field_name . '_slider'] = array(
        '#type' => 'select',
        '#title' => t("Select Slider:"),
        '#options' => $options,
        '#default_value' => variable_get($field_name . '_slider', ''),
        '#required' => FALSE,
      );
      $form['fcs_enabled_to'][$content_type][$field_name . '_jsconfig'] = array(
        '#type' => 'textfield',
        '#title' => t("Configuration:"),
        '#description' => t("width:'100%', height: '100%'"),
        '#size' => 60,
        '#default_value' => variable_get($field_name . '_jsconfig', ''),
        '#required' => FALSE,
      );
    }
  }

  return system_settings_form($form);
}
